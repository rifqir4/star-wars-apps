import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:star_wars_app/sources/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}
