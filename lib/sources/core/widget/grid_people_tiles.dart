import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/constant/route.dart';
import 'package:star_wars_app/sources/core/model/people.dart';

class GridPeopleTiles extends StatelessWidget {
  const GridPeopleTiles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PeopleCubit, PeopleState>(builder: (context, state) {
      if (!(state is PeopleLoaded)) return CircularProgressIndicator();

      List<People> listPeople = state.listPeople;
      return GridView.builder(
        itemCount: listPeople.length,
        itemBuilder: (context, idx) => Card(
          child: InkWell(
            onTap: () => Navigator.pushNamed(context, MyRoute.homedetail, arguments: listPeople[idx]),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    onPressed: () => context.read<PeopleCubit>().changeFavorite(listPeople[idx]),
                    icon: Icon(
                      Icons.favorite,
                      color: listPeople[idx].isfav ? Colors.red : Colors.grey,
                    ),
                  ),
                ),
                Icon(Icons.person, size: 30, color: Colors.grey),
                SizedBox(height: 5),
                Text(listPeople[idx].name),
              ],
            ),
          ),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 1, mainAxisSpacing: 12, crossAxisSpacing: 12),
      );
    });
  }
}
