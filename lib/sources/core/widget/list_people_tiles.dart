import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/constant/route.dart';
import 'package:star_wars_app/sources/core/model/people.dart';

class ListPeopleTiles extends StatelessWidget {
  const ListPeopleTiles({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PeopleCubit, PeopleState>(
      builder: (context, state) {
        if (!(state is PeopleLoaded)) return CircularProgressIndicator();

        List<People> listPeople = state.listPeople;
        return ListView.builder(
          itemCount: listPeople.length,
          itemBuilder: (context, idx) => ListTile(
            onTap: () => Navigator.pushNamed(context, MyRoute.homedetail, arguments: listPeople[idx]),
            leading: Icon(Icons.person),
            title: Text(listPeople[idx].name),
            trailing: IconButton(
              onPressed: () {
                context.read<PeopleCubit>().changeFavorite(listPeople[idx]);
              },
              icon: Icon(
                Icons.favorite,
                color: listPeople[idx].isfav ? Colors.red : Colors.grey,
              ),
            ),
          ),
        );
      },
    );
  }
}
