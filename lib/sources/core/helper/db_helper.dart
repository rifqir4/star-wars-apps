import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBHelper {
  static DBHelper? _dbHelper;
  static Database? _database;

  static final _dbName = 'myDatabase.db';
  static int _dbVersion = 1;
  static String _dbBackupDate = '';
  static String _dbRestoreDate = '';

  int get version => _dbVersion;
  String get restoreDate => _dbRestoreDate;
  String get backupDate => _dbBackupDate;

  DBHelper._createInstance();

  factory DBHelper() {
    if (_dbHelper == null) {
      _dbHelper = DBHelper._createInstance();
    }
    return _dbHelper ?? DBHelper._createInstance();
  }

  Future<Database?> get database async {
    if (_database != null) return _database;

    _database = await _initiateDatabase();
    return _database;
  }

  Future<String> _databasePath() async {
    String databasePath = await getDatabasesPath();
    String path = join(databasePath, _dbName);
    return path;
  }

  Future<Database> _initiateDatabase() async {
    String path = await _databasePath();
    return await openDatabase(path, version: _dbVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    var batch = db.batch();

    batch.execute('''
      CREATE TABLE IF NOT EXISTS t_people(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name VARCHAR(255) UNIQUE NOT NULL,
        height INT,
        mass INT,
        hair_color VARCHAR,
        skin_color VARCHAR,
        eye_color VARCHAR,
        birth_year VARCHAR,
        gender VARCHAR,
        films TEXT,
        species TEXT,
        created VARCHAR,
        edited VARCHAR
      )
    ''');

    await batch.commit();
  }

  Future<int> delete(String table, {required String id}) async {
    Database? db = await this.database;
    return await db!.delete(table, where: 'id = ?', whereArgs: [id]);
  }

  Future<int> insert({required String table, required Map<String, dynamic> row}) async {
    Database? db = await this.database;
    return await db!.insert(table, row);
  }

  Future<int> update(String table, {required Map<String, dynamic> row, required String id}) async {
    Database? db = await this.database;
    return await db!.update(table, row, where: "id = ?", whereArgs: [id]);
  }

  Future<List<Map<String, dynamic>>> fetchAll(String table) async {
    Database? db = await this.database;
    return await db!.query(table, orderBy: 'name');
  }

  Future<Map<String, dynamic>> fetch(String table, {required String col, required String val}) async {
    Database? db = await this.database;
    final result = await db!.query(table, where: '$col = ?', whereArgs: [val]);
    return result[0];
  }

  Future<List<Map<String, dynamic>>> raw(sql, {List? args}) async {
    Database? db = await this.database;
    return await db!.rawQuery(sql, args);
  }
}
