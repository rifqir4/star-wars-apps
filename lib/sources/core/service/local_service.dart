import 'package:sqflite/sqflite.dart';
import 'package:star_wars_app/sources/core/helper/db_helper.dart';
import 'package:star_wars_app/sources/core/model/people.dart';

class LocalService {
  static final _dbHelper = DBHelper();

  static Future<dynamic> fetchPeople() async {
    try {
      final result = await _dbHelper.fetchAll('t_people');
      return result;
    } on DatabaseException {
      return null;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  static Future<void> insertPeople(People people) async {
    try {
      await _dbHelper.insert(table: 't_people', row: people.toDatabaseInsertJson());
    } on DatabaseException {} catch (e) {
      print(e.toString());
    }
  }
}
