import 'package:dio/dio.dart';

class ApiService {
  static final String apiUrl = "https://swapi.dev/api/";

  static Future<dynamic> fetchPeople() async {
    try {
      final responseApi = await Dio().get("$apiUrl/people");
      if (responseApi.statusCode != 200) throw "Terjadi kesalahan saat mengambil data";
      return responseApi.data['results'];
    } catch (e) {
      print(e);
    }
  }

  static Future<dynamic> fetchFilmDetail(String url) async {
    try {
      final responseApi = await Dio().get(url);
      if (responseApi.statusCode != 200) throw "Terjadi kesalahan saat mengambil data";
      return responseApi.data;
    } catch (e) {
      print(e);
    }
  }

  static Future<dynamic> fetchSpeciesDetail(String url) async {
    try {
      final responseApi = await Dio().get(url);
      if (responseApi.statusCode != 200) throw "Terjadi kesalahan saat mengambil data";
      return responseApi.data;
    } catch (e) {
      print(e);
    }
  }
}
