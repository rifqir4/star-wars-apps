import 'package:flutter_bloc/flutter_bloc.dart';

class TypeCubit extends Cubit<String> {
  TypeCubit() : super("list");

  void toGrid() => emit('grid');

  void toList() => emit('list');
}
