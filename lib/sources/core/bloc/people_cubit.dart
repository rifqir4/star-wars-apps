import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/model/people.dart';
import 'package:star_wars_app/sources/core/service/local_service.dart';

part 'people_state.dart';

class PeopleCubit extends Cubit<PeopleState> {
  PeopleCubit() : super(PeopleInitial());

  Future<void> loadPeople() async {
    List<People> _listPeople = [];
    final List responseData = await LocalService.fetchPeople();
    for (var i = 0; i < responseData.length; i++) {
      _listPeople.add(People.fromJson(responseData[i]));
    }
    emit(PeopleLoaded(listPeople: _listPeople));
  }

  void changeFavorite(People people) {
    people.isfav = !people.isfav;
    updatePeopleList();
  }

  void updatePeopleList() {
    final currentState = state;
    if (currentState is PeopleLoaded) emit(PeopleLoaded(listPeople: currentState.listPeople));
  }

  void editPeopleName(People people, String name) {
    people.name = name;
    updatePeopleList();
  }

  void sortAsc() {
    final currentState = state;
    if (currentState is PeopleLoaded) {
      final listPeople = currentState.listPeople;
      listPeople.sort((a, b) => a.name.toLowerCase().compareTo(b.name.toLowerCase()));
      emit(PeopleLoaded(listPeople: listPeople));
    }
  }

  void sortDesc() {
    final currentState = state;
    if (currentState is PeopleLoaded) {
      final listPeople = currentState.listPeople;
      listPeople.sort((a, b) => b.name.toLowerCase().compareTo(a.name.toLowerCase()));
      emit(PeopleLoaded(listPeople: listPeople));
    }
  }

  void addPeople(People people) {
    final currentState = state;
    if (currentState is PeopleLoaded) {
      final listPeople = currentState.listPeople;
      listPeople.add(people);
      emit(PeopleLoaded(listPeople: listPeople));
    }
  }

  void deletePeople(People people) {
    final currentState = state;
    if (currentState is PeopleLoaded) {
      final listPeople = currentState.listPeople.where((element) => element.id != people.id).toList();
      emit(PeopleLoaded(listPeople: listPeople));
    }
  }
}
