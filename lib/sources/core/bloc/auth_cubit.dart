import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthCubit extends Cubit<bool> {
  AuthCubit() : super(false);

  void checkLogin() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    final islogin = sharedPreferences.getBool('isLogin') ?? false;

    if (islogin)
      emit(true);
    else
      emit(false);
  }

  void login() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("isLogin", true);
    emit(true);
  }

  void logout() async {
    final sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setBool("isLogin", false);
    sharedPreferences.clear();
    emit(false);
  }
}
