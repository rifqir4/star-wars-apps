part of 'people_cubit.dart';

abstract class PeopleState {}

class PeopleInitial extends PeopleState {}

class PeopleError extends PeopleState {}

class PeopleLoaded extends PeopleState {
  final List<People> listPeople;
  PeopleLoaded({required this.listPeople});
}
