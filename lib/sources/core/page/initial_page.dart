import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/model/people.dart';
import 'package:star_wars_app/sources/core/page/error_page.dart';
import 'package:star_wars_app/sources/core/page/loading_page.dart';
import 'package:star_wars_app/sources/core/page/main_page.dart';
import 'package:star_wars_app/sources/core/service/api_service.dart';
import 'package:star_wars_app/sources/core/service/local_service.dart';

class InitialPage extends StatelessWidget {
  const InitialPage({Key? key}) : super(key: key);

  Future<void> _initialFuture(BuildContext context) async {
    final _sharedPreferences = await SharedPreferences.getInstance();
    final isInitialized = _sharedPreferences.containsKey("isInitialized") ? _sharedPreferences.getBool("isInitialized") : false;

    if (isInitialized != true) {
      try {
        final List responseData = await ApiService.fetchPeople();
        for (var i = 0; i < responseData.length; i++) {
          final People people = People.fromJson(responseData[i]);
          await LocalService.insertPeople(people);
        }
        _sharedPreferences.setBool('isInitialized', true);
      } catch (e) {
        _sharedPreferences.setBool('isInitialized', false);
        throw "Terjadi Kesalahan";
      }
    }
    context.read<PeopleCubit>().loadPeople();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialFuture(context),
      builder: (context, snap) {
        if (snap.connectionState != ConnectionState.done) return LoadingPages();
        if (snap.hasError) return ErrorPage();
        return MainPage();
      },
    );
  }
}
