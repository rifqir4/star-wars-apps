import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/page_cubit.dart';
import 'package:star_wars_app/sources/features/favorite/favorite_page.dart';
import 'package:star_wars_app/sources/features/home/home_page.dart';
import 'package:star_wars_app/sources/features/profile/profile_page.dart';

class MainPage extends StatelessWidget {
  MainPage({Key? key}) : super(key: key);

  final List<Widget> pageList = [
    HomePage(),
    FavoritePage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PageCubit, int>(
      builder: (context, state) => Scaffold(
        body: pageList[state],
        bottomNavigationBar: BottomNavigationBar(
          onTap: (idx) => context.read<PageCubit>().changeScreenIndex(idx),
          currentIndex: state,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(icon: Icon(Icons.favorite), label: "Favorite"),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
          ],
        ),
      ),
    );
  }
}
