class People {
  final int id;
  String name;
  final int height;
  final int mass;
  final String hairColor;
  final String skinColor;
  final String eyeColor;
  final String birthYear;
  final String gender;
  final List<String> films;
  final List<String> species;
  final String created;
  final String edited;
  bool isfav;

  People({
    required this.id,
    required this.name,
    required this.height,
    required this.mass,
    required this.hairColor,
    required this.skinColor,
    required this.eyeColor,
    required this.birthYear,
    required this.gender,
    required this.films,
    required this.species,
    required this.created,
    required this.edited,
    required this.isfav,
  });

  factory People.fromJson(Map<String, dynamic> json) => People(
        id: json['id'] ?? 0,
        name: json['name'],
        height: int.parse(json['height'].toString()),
        mass: int.parse(json['mass'].toString()),
        hairColor: json['hair_color'],
        skinColor: json['skin_color'],
        eyeColor: json['eye_color'],
        birthYear: json['birth_year'],
        gender: json['gender'],
        films: (json['films'] is String) ? json['films'].split(",").toList() : List<String>.from(json['films']),
        species: (json['species'] is String) ? json['species'].split(",").toList() : List<String>.from(json['species']),
        created: json['created'],
        edited: json['edited'],
        isfav: json['isFav'] ?? false,
      );

  toDatabaseInsertJson() => {
        'name': name,
        'height': height,
        'mass': mass,
        'hair_color': hairColor,
        'skin_color': skinColor,
        'eye_color': eyeColor,
        'birth_year': birthYear,
        'gender': gender,
        'films': films.join(","),
        'species': species.join(","),
        'created': created,
        'edited': edited,
      };
}
