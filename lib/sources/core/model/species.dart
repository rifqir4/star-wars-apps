class Species {
  final String name;
  final String classification;
  final String lifeSpan;

  Species({required this.name, required this.classification, required this.lifeSpan});

  factory Species.fromJson(Map<String, dynamic> json) => Species(
        name: json['name'] ?? "-",
        classification: json['classification'] ?? "-",
        lifeSpan: json['average_lifespan'] ?? "-",
      );
}
