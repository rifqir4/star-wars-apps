class Film {
  final String title;
  final String prolog;
  final String director;
  final String producer;
  final String releaseDate;

  Film({required this.title, required this.prolog, required this.director, required this.producer, required this.releaseDate});

  factory Film.fromJson(Map<String, dynamic> json) => Film(
        title: json['title'] ?? '-',
        prolog: json['opening_crawl'] ?? '-',
        director: json['director'] ?? '-',
        producer: json['producer'] ?? '-',
        releaseDate: json['release_date'] ?? '-',
      );
}
