import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future<Map> loadProfile() async {
      final sharedPreferences = await SharedPreferences.getInstance();

      return {
        "name": sharedPreferences.getString('name'),
        "photoUrl": sharedPreferences.getString('photoUrl'),
        "email": sharedPreferences.getString('email'),
      };
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: FutureBuilder<Map>(
        future: loadProfile(),
        builder: (context, snap) {
          if (snap.connectionState != ConnectionState.done)
            return Center(
              child: Text("Loading.."),
            );
          if (snap.data == null)
            return Center(
              child: Text("Tidak ada data"),
            );
          Map data = snap.data ?? {"name": "", "photoUrl": "", "email": ""};
          return Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                CircleAvatar(
                  backgroundImage: NetworkImage(data['photoUrl']),
                  radius: 35,
                ),
                SizedBox(height: 12),
                Text(
                  data['name'],
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(data['email']),
              ],
            ),
          );
        },
      ),
    );
  }
}
