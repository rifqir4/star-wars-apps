import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/model/film.dart';
import 'package:star_wars_app/sources/core/model/people.dart';
import 'package:star_wars_app/sources/core/model/species.dart';
import 'package:star_wars_app/sources/core/service/api_service.dart';

class HomeDetailPage extends StatelessWidget {
  final People people;
  HomeDetailPage(this.people, {Key? key}) : super(key: key);

  final TextEditingController _editingController = new TextEditingController();

  Future<Film> _futureFetchFilm(String url) async {
    try {
      final responseData = await ApiService.fetchFilmDetail(url);
      return Film.fromJson(responseData);
    } catch (e) {
      throw "Terjadi Kesahalah";
    }
  }

  Future<Species> _futureFetchSpecies(String url) async {
    try {
      if (url == "") throw "Data Kosong";
      final responseData = await ApiService.fetchSpeciesDetail(url);
      return Species.fromJson(responseData);
    } catch (e) {
      throw "Terjadi Kesahalah";
    }
  }

  @override
  Widget build(BuildContext context) {
    _editingController.text = people.name;
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail People"),
        actions: [
          IconButton(
            onPressed: () async {
              context.read<PeopleCubit>().editPeopleName(people, _editingController.text);
              await showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  content: Text("Berhasil mengubah nama"),
                  actions: [TextButton(onPressed: () => Navigator.pop(context), child: Text("Ok"))],
                ),
              );
              Navigator.pop(context);
            },
            icon: Icon(Icons.save),
          ),
          IconButton(
            onPressed: () async {
              final bool resutlt = await showDialog(
                barrierDismissible: false,
                context: context,
                builder: (context) => AlertDialog(
                  content: Text("Yakin menghapus data?"),
                  actions: [
                    TextButton(onPressed: () => Navigator.pop(context, true), child: Text("Ok")),
                    TextButton(onPressed: () => Navigator.pop(context, false), child: Text("Cancel")),
                  ],
                ),
              );
              if (resutlt) {
                context.read<PeopleCubit>().deletePeople(people);
                Navigator.pop(context);
              }
            },
            icon: Icon(Icons.delete),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ListTile(
              title: Text("Name"),
              subtitle: TextFormField(
                controller: _editingController,
              ),
            ),
            ListTile(
              title: Text("Gender"),
              subtitle: Text(people.gender),
            ),
            ListTile(
              title: Text("Heigth"),
              subtitle: Text(people.height.toString()),
            ),
            ListTile(
              title: Text("Mass"),
              subtitle: Text(people.mass.toString()),
            ),
            Text("Films List", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
            Container(
              padding: const EdgeInsets.all(12),
              height: 400,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: people.films.length,
                itemBuilder: (context, idx) => Container(
                  margin: const EdgeInsets.only(right: 12),
                  padding: const EdgeInsets.all(12),
                  width: MediaQuery.of(context).size.width * 0.8,
                  decoration: BoxDecoration(color: Colors.grey.shade900),
                  child: FutureBuilder<Film>(
                    future: _futureFetchFilm(people.films[idx]),
                    builder: (context, snap) {
                      if (snap.connectionState != ConnectionState.done) return Center(child: CircularProgressIndicator());
                      if (snap.data == null) return Center(child: Text("Terjadi Kesalahan", style: TextStyle(color: Colors.white)));
                      return SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(snap.data!.title, style: TextStyle(color: Colors.white, fontSize: 20)),
                            Text(snap.data!.releaseDate, style: TextStyle(color: Colors.white)),
                            SizedBox(height: 12),
                            Text("Director: ${snap.data!.director}", style: TextStyle(color: Colors.white)),
                            Text("Producer: ${snap.data!.producer}", style: TextStyle(color: Colors.white)),
                            SizedBox(height: 12),
                            Text(snap.data!.prolog, style: TextStyle(color: Colors.white)),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            Text("Species List", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
            Container(
              height: 150,
              child: people.species.length == 0
                  ? Center(
                      child: Text("Kosong"),
                    )
                  : ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: people.species.length,
                      itemBuilder: (context, idx) => Container(
                        margin: const EdgeInsets.only(right: 12),
                        padding: const EdgeInsets.all(12),
                        width: MediaQuery.of(context).size.width * 0.5,
                        decoration: BoxDecoration(color: Colors.grey.shade900),
                        child: FutureBuilder<Species>(
                          future: _futureFetchSpecies(people.species[idx]),
                          builder: (context, snap) {
                            if (snap.connectionState != ConnectionState.done) return Center(child: CircularProgressIndicator());
                            if (snap.data == null) return Center(child: Text("Terjadi Kesalahan", style: TextStyle(color: Colors.white)));
                            return SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(snap.data!.name, style: TextStyle(color: Colors.white, fontSize: 20)),
                                  SizedBox(height: 12),
                                  Text("Classification: ${snap.data!.classification}", style: TextStyle(color: Colors.white)),
                                  Text("Lifespan: ${snap.data!.lifeSpan}", style: TextStyle(color: Colors.white)),
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
