import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/auth_cubit.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/bloc/type_cubit.dart';
import 'package:star_wars_app/sources/core/model/people.dart';
import 'package:star_wars_app/sources/core/widget/grid_people_tiles.dart';
import 'package:star_wars_app/sources/core/widget/list_people_tiles.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final TextEditingController _editingController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TypeCubit, String>(
      builder: (context, type) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Home"),
            actions: [
              IconButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                      content: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text("Tambah data"),
                          TextFormField(
                            controller: _editingController,
                            decoration: InputDecoration(hintText: "New Name"),
                          ),
                        ],
                      ),
                      actions: [
                        TextButton(
                            onPressed: () {
                              final newPeople = People(
                                id: DateTime.now().millisecond,
                                name: _editingController.text,
                                birthYear: "123BBY",
                                gender: "male",
                                height: 100,
                                mass: 250,
                                isfav: false,
                                hairColor: "red",
                                eyeColor: "red",
                                skinColor: "brown",
                                films: [],
                                species: [],
                                edited: DateTime.now().toString(),
                                created: DateTime.now().toString(),
                              );
                              context.read<PeopleCubit>().addPeople(newPeople);
                              Navigator.pop(context, false);
                            },
                            child: Text("Tambah")),
                        TextButton(onPressed: () => Navigator.pop(context, false), child: Text("Cancel")),
                      ],
                    ),
                  );
                },
                icon: Icon(Icons.person_add),
              ),
              PopupMenuButton(
                icon: Icon(Icons.sort_by_alpha),
                itemBuilder: (context) => [
                  PopupMenuItem(child: Text("Ascending"), value: "asc"),
                  PopupMenuItem(child: Text("Descending"), value: "desc"),
                ],
                onSelected: (val) {
                  if (val == 'asc') context.read<PeopleCubit>().sortAsc();
                  if (val == 'desc') context.read<PeopleCubit>().sortDesc();
                },
              ),
              IconButton(
                onPressed: () => context.read<AuthCubit>().logout(),
                icon: Icon(Icons.logout),
              ),
            ],
          ),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: type == "grid" ? GridPeopleTiles() : ListPeopleTiles(),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () => type == "grid" ? context.read<TypeCubit>().toList() : context.read<TypeCubit>().toGrid(),
            child: Icon(type == "grid" ? Icons.grid_3x3 : Icons.list),
          ),
        );
      },
    );
  }
}
