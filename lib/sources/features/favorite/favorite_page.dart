import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/constant/route.dart';
import 'package:star_wars_app/sources/core/model/people.dart';

class FavoritePage extends StatelessWidget {
  const FavoritePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorite"),
      ),
      body: BlocBuilder<PeopleCubit, PeopleState>(builder: (context, state) {
        if (!(state is PeopleLoaded)) return CircularProgressIndicator();

        List<People> _filtered = state.listPeople.where((element) => element.isfav).toList();
        return ListView.builder(
          itemCount: _filtered.length,
          itemBuilder: (context, idx) => ListTile(
            onTap: () => Navigator.pushNamed(context, MyRoute.homedetail, arguments: _filtered[idx]),
            leading: Icon(Icons.person),
            title: Text(_filtered[idx].name),
            trailing: IconButton(
              onPressed: () => context.read<PeopleCubit>().changeFavorite(_filtered[idx]),
              icon: Icon(
                Icons.favorite,
                color: _filtered[idx].isfav ? Colors.red : Colors.grey,
              ),
            ),
          ),
        );
      }),
    );
  }
}
