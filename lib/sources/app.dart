import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:star_wars_app/sources/core/bloc/auth_cubit.dart';
import 'package:star_wars_app/sources/core/bloc/page_cubit.dart';
import 'package:star_wars_app/sources/core/bloc/people_cubit.dart';
import 'package:star_wars_app/sources/core/bloc/type_cubit.dart';
import 'package:star_wars_app/sources/core/constant/route.dart';
import 'package:star_wars_app/sources/core/model/people.dart';
import 'package:star_wars_app/sources/core/page/initial_page.dart';
import 'package:star_wars_app/sources/features/auth/auth_page.dart';
import 'package:star_wars_app/sources/features/home/home_detail_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => AuthCubit()),
        BlocProvider(create: (_) => PeopleCubit()),
        BlocProvider(create: (_) => PageCubit()),
        BlocProvider(create: (_) => TypeCubit()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Star Wars App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: (RouteSettings routeSettings) {
          return MaterialPageRoute(
            settings: routeSettings,
            builder: (BuildContext context) {
              switch (routeSettings.name) {
                case MyRoute.homedetail:
                  return HomeDetailPage(routeSettings.arguments as People);
                default:
                  return Wrapper();
              }
            },
          );
        },
      ),
    );
  }
}

class Wrapper extends StatelessWidget {
  const Wrapper({Key? key}) : super(key: key);

  Future<void> checkLogin(BuildContext context) async {
    context.read<AuthCubit>().checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: checkLogin(context),
      builder: (context, snap) => BlocBuilder<AuthCubit, bool>(
        builder: (context, isLogin) => isLogin ? InitialPage() : AuthPage(),
      ),
    );
  }
}
